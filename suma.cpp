#include <iostream>
#include <cstdlib>

using namespace std;

int main(int argc, char const *argv[])
{
	int a=0,b=0;

	if (argc >= 2)
	{
		a = atoi(argv[1]);
		b = atoi(argv[2]);
	}else{
		cin >> a >> b;
	}
	
	cout << a+b;
	return 0;
}