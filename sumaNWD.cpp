#include <iostream>
#include <cstdio>
#include <vector>
using namespace std;

int NWD(int a, int b)
{
    while( a != b )
    {
	    if( a > b )
			 a = a - b;
		else
	    	b = b -a;
    }
    return a;
}
int main() {
	int liczba = -1, nwd = 0;
	vector <int> liczby;
	
	while(liczba != 0){
		cin >> liczba;
		if(liczba == 1){
			nwd += NWD(liczby.at(liczby.size()-1), liczby.at(liczby.size()-2));
		}else{
			liczby.push_back(liczba);
		}
	}
	cout << nwd;
	return 0;
}