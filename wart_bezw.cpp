#include <iostream>
#include <math.h>
#include <cstdlib>

using namespace std;

int main(int argc, char const *argv[])
{
	int a = 0;

	if(argc >= 1){
		a = atoi(argv[1]);
	}else{
		cin >> a;
	}
	cout << abs(a);
	return 0;
}