#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <math.h>
#include <string>

using namespace std;

short int getNmbFromPos(uint _times, char const **pos, string &_sequence, uint &_pow){
	char *str;
	for(int i = 1; i <= _times; i++){
		if(*pos[i] > _sequence.length()){
			//_sequence += itoa(pow(10,_pow+1), _sequence, 10);
			_sequence += sprintf(str, "%f", pow(10,_pow+1));
			_pow++;
		}

		return atoi(_sequence[atoi(pos[i-1])]);
	}
	return 0;	
}
int main(int argc, char const *argv[])
{
	string sequence = "1";
	uint times = 0, pos = 0, pow = 0;

	if (argc >= 2)
	{
		times = atoi(argv[1]);
		if(times > 0)
			cout << getNmbFromPos(times, argv, sequence, pow) << " ";
	}

	return 0;
}