#include "iostream"
#include "vector"

using namespace std;

int main(int argc, char const *argv[])
{
	vector <int> oceny;
	int nmbr;
	float tmp = 0;
	cout.precision(2);

	while(nmbr != -1){
		tmp = 0;
		cin >> nmbr;
		if(nmbr == 1){
			for(int i = 0; i < oceny.size(); i++){
				tmp += oceny.at(i);
			}
			cout << fixed << tmp/oceny.size() << endl;
		}else if(nmbr == 0){
			for(int i = 0; i < oceny.size(); i++){
				cout << oceny.at(i) << " ";
			}
			cout << endl;
		}else{
			oceny.push_back(nmbr);
		}
	}
	return 0;
}