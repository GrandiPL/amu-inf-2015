#include "iostream"

using namespace std;

int main(int argc, char const *argv[])
{
	int a,b,vote, max = 0;
	cin >> a >> b;

	int *tab = new int[a];

	for(int i = 0; i < b; i++){
		cin >> vote;
		tab[vote-1] += 1;
	}
	for(int i = 0; i < a; i++){
		cout << i+1 << ": " << tab[i] << endl;
		if(tab[i]>tab[max])
			max=i;
	}
	cout << max+1;
	return 0;
}