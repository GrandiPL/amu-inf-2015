#include "iostream"

using namespace std;

int main(int argc, char const *argv[])
{
	int w, k, nmbr;

	cin >> w >> k;

	int **tab = new int * [w];

	for(int i = 0; i < w; i++){
		tab[i] = new int[k];
		for(int x = 0; x < k; x++){
			cin >> nmbr;
			tab[i][x] = nmbr;
		}
	}
	for(int x = k-1; x >= 0; x--){
		for(int i = 0; i < w; i++){
			cout << tab[i][x] << ' ';
		}
		cout << endl;
	}
	return 0;
}