#include <iostream>
#include <vector>

using namespace std;

int main() {
	int boxes, rows, treasure, sum, first, last;
	vector < int > box;
	
	cin >> boxes;
	
	for(int i = 0; i < boxes; i++){
		cin >> treasure;
		box.push_back(treasure);
	}
	cin >> rows;
	for(int i = 0; i < rows; i++){
		sum = 0;
		cin >> first >> last;
		for(int x = first-1; x < last; x++){
			sum += box.at(x);
		}
		cout << sum << endl;
	}
	return 0;
}